#include <stdio.h>
#include <stdlib.h>
#include <time.h>

int generator() {
  double rnd;
  rnd = (rand() % 2) * 2 - 1;

  return rnd;
}

double gen2() {

  // ---------- settings ------------
  int max_step =
      1000; // ограничение по числу шагов,
            // т.к после некоторого количества опыт становиться не актульным
  int lenght[] = {10, 15, 20, 25, 30};
  int distance = 0, interval = 0, cnt = 0;
  // --------------------------------
  while (1) {
    interval = 0;
    cnt = 0;
    while ((interval <= lenght[4]) && (cnt < max_step - 1)) {
      interval += generator();
      cnt++;
    }
    if (cnt < max_step - 1)
      return cnt;
  }
}

void hist(double gen(), double width, int cols, int dots) {
  int sums[cols];
  for (int c = 0; c < cols; sums[c++] = 0)
    ;
  double x = 0.0;
  for (int i = 0; i < 1; i++) {
    for (int j = 0; j < dots; j++) {
      x = gen() / width;
      sums[(int)(x * cols)]++;
    }
  }

  double wdt = width / cols;
  for (int cnt = 0; cnt < cols; cnt++) {
    x = wdt * (0.5 + cnt);
    printf("%f\t%i\t%f\n", x, sums[cnt], wdt * 0.8);
  }
}

int main() {
  srand(time(NULL));
  int cols = 143;         // кол-во столбиков
  double dots = 10000000; // кол-во опытов
  double width = 1000; // ширина диапазона распределения

  hist(gen2, width, cols, dots);
  return 0;
}
