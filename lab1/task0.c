#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#define maxim 13

double f(double x) {
  if (x <= 2) {
    return 3 * x + 5;
  } else if (x <= 4) {
    return 11;
  } else {
    return x + 7;
  }
}
double generator(double f1(double x)) {
  double rnd1, rnd2;
  int a = 0, b = 6;
  while (1) {
    rnd1 = ((double)rand() / RAND_MAX) * (b - a) + a;
    rnd2 = ((double)rand() / RAND_MAX) * maxim;
    if (rnd2 <= f1(rnd1)) {
      return rnd1;
    }
  }
}

int main() {
  long quantity = 57548146;
  int val;
  int sums[143] = {0};

  for (long i = 0; i < quantity; i++) {
    val = (int)(generator(f) / 6 * 143);
    sums[val]++;
  }

  for (int cnt = 0; cnt < 143; cnt++) {
    printf("%i\t%i\t%lf\n", cnt, sums[cnt], (double)sums[cnt] / quantity);
  }

  return 0;
}
