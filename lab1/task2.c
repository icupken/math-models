#include <stdio.h>
#include <stdlib.h>
#include <time.h>

int generator() {
  double rnd;
  rnd = (rand() % 2) * 2 - 1;

  return rnd;
}

double gen2() {

  // ---------- variables ------------
  int x = 0, y = 0; // координаты
  int r = 5;        // радиус
  int cnt = 0;
  // --------------------------------
  while ((x-0)*(x-0)+(y-0)*(y-0) <= r*r) {
    x += generator();
    y += generator(); 
    cnt++;
  }
  return cnt;
}

void hist(double gen(), double width, int cols, int dots) {
  int sums[cols];
  for (int c = 0; c < cols; sums[c++] = 0);
  double x = 0.0;
  for (int i = 0; i < 1; i++) {
    for (int j = 0; j < dots; j++) {
      x = gen() / width;
      if (x < 1) {
        sums[(int)(x * cols)]++;
      }
    }
  }
  double wdt = width / cols;
  for (int cnt = 0; cnt < cols; cnt++) {
    x = wdt * (0.5 + cnt);
    printf("%f\t%i\t%f\n", x, sums[cnt], wdt * 0.8);
  }
}

int main() {
  srand(time(NULL));
  int cols = 500;         // кол-во столбиков
  double dots = 100000000; // кол-во опытов
  double width = 100;   // ширина диапазона распределения

  hist(gen2, width, cols, dots);
  return 0;
}
